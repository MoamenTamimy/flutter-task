import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_task/bloc/user_bloc.dart';
import 'package:flutter_task/data/repository/UserRepository.dart';

import 'data/model/ResultObject.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.amber,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: BlocProvider(
        create: (context) =>
            UserBloc(UserRepositoryImp())..add(FetchUserEvent()),
        child: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: BlocBuilder<UserBloc, UserState>(builder: (context, state) {
          if (state is UserLoading) {
            return ProgressScreen();
          } else if (state is UserLoaded) {
            return UserScreen(
                "${state.result.name.first} ${state.result.name.last}",
                state.result.email,
                "${state.result.location.country},${state.result.location.city}",
            state.result.picture.large);
          }else if (state is UserError) {
            return ErrorScreen(state.message);
          }
          return Container();
        }),
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.red, Colors.amber])),
      ),
    );
  }
}

class ProgressScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Expanded(
                child: Text(
              "Loading data from API...",
              style: TextStyle(color: Colors.white, fontSize: 20),
            )),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation(Colors.white),
            ),
          ),
        ],
      ),
    );
  }
}

class UserScreen extends StatelessWidget {
  final String userName;
  final String userEmail;
  final String userAddress;
  final String userImgUrl;

  UserScreen(this.userName, this.userEmail, this.userAddress,this.userImgUrl);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CircleImage(userImgUrl),
          Text(
            userName,
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          Text(
            userEmail,
            style: TextStyle(color: Colors.white, fontSize: 15),
          ),
          Text(
            userAddress,
            style: TextStyle(color: Colors.white, fontSize: 10),
          ),
        ],
      ),
    );
  }
}

class ErrorScreen extends StatelessWidget {
  final String _message;
  ErrorScreen(this._message);
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Center(
            child: Expanded(
                child: Text(_message,
                    style: TextStyle(color: Colors.white, fontSize: 20)))));
  }
}

class CircleImage extends StatelessWidget {
  final String _imgUrl;

  CircleImage(this._imgUrl);
  @override
  Widget build(BuildContext context) {
    double _size = 180.0;

    return Center(
      child: new Container(
          width: _size,
          height: _size,
          decoration: new BoxDecoration(
              shape: BoxShape.circle,
              image: new DecorationImage(
                  fit: BoxFit.cover,
                  image: new NetworkImage(
                      _imgUrl)))),
    );
  }
}
