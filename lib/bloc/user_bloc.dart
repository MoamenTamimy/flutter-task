import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_task/data/model/ResultObject.dart';
import 'package:flutter_task/data/repository/UserRepository.dart';
import 'package:meta/meta.dart';

part 'user_event.dart';

part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  final UserRepository _repository;
  UserBloc(this._repository) : super(UserInitial());

  @override
  Stream<UserState> mapEventToState(
    UserEvent event,
  ) async* {
    if (event is FetchUserEvent) {
      yield UserLoading();
      try {
        final result = await _repository.fetchUser();
        yield UserLoaded(result);
      } catch (e) {
        yield UserError(e.toString());
      }
    }
  }
}
