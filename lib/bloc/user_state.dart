part of 'user_bloc.dart';

@immutable
abstract class UserState {}

class UserInitial extends UserState {}
class UserLoading extends UserState {}
class UserLoaded extends UserState {
  Result result;
  UserLoaded(this.result);
}
class UserError extends UserState {
  String message;
  UserError(this.message);
}
