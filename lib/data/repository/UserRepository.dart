import 'package:dio/dio.dart';
import 'package:flutter_task/data/model/ResultObject.dart';

abstract class UserRepository {
  Future<Result> fetchUser();
}

class UserRepositoryImp implements UserRepository {
  @override
  Future<Result> fetchUser() async {
    print("LALALALALALALALALA ");
    Response response = await Dio().get("https://randomuser.me/api/");
      Map responseBody = response.data;
      print("LALALALALALALALALA ${responseBody}");
      return ResultObj.fromMap(responseBody).results[0];
  }
}
